#!/bin/bash

if [[ -f ~/.bashrc ]]; then
    profile=".bashrc"
else
    profile=".profile"
fi

if [[ ! -f ~/"$profile" ]]; then
    if [[ -f /etc/skel/"$profile" ]]; then
        cp /etc/skel/"$profile" ~/
    else
        touch ~/"$profile"
    fi
fi

if [[ -z $(grep "export SCRIPTSPATH=" ~/"$profile") ]]; then
    echo "export SCRIPTSPATH=${0%/*}
export PATH=\$SCRIPTSPATH:\$PATH
chmod +x \$SCRIPTSPATH/*.sh" | tee -a ~/"$profile"

    source ~/"$profile"
fi