#!/bin/bash

if [[ ! -d ~/.local/share/gnome-shell/extensions/dash-to-dock@micxgx.gmail.com ]]; then
    git clone https://github.com/micheleg/dash-to-dock.git
    cd dash-to-dock
    make
    make install
    cd ../
    rm -rf dask-to-dock
fi