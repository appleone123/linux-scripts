sudo wget -O /boot/efi/shellx64.efi https://github.com/tianocore/edk2/blob/UDK2018/EdkShellBinPkg/FullShell/X64/Shell_Full.efi
echo "mm 02000004 1 ;PCI :7
mm 0017003E 1 ;PCI :8

for %i run (0 9)
  if exist fs%i:\EFI\<UUID_ROOT>\vmlinuz.efi then
    fs%i:\EFI\<UUID_ROOT>\vmlinuz.efi root=<INDENTIFER_ROOT> rw initrd=\EFI\<UUID_ROOT>\initrd.img
  endif
endfor" | sudo tee /boot/efi/startup.nsh

sudo efibootmgr --create --disk <INDENTIFER_DISK> --loader shellx64.efi --label "EFI Shell"
sudo efibootmgr -o $(efibootmgr | grep "EFI Shell" | sed 's/Boot//'| sed 's/\*.*//')

echo 'Section "Device"
    Identifier     "Device0"
    Option         "RegistryDwords" "EnableBrightnessControl=1"
EndSection' | sudo tee /usr/share/X11/xorg.conf.d/10-brightness.conf

echo 'Section "Screen"
    Identifier     "Screen0"
    Option         "NoLogo" "True"
EndSection' | sudo tee /usr/share/X11/xorg.conf.d/10-nologo.conf