#!/bin/bash

sudo apt install cifs-utils -y &>/dev/null

TIMECAPSULE_IP="time-capsule.local"
TIMECAPSULE_VOLUME="/Time Capsule"
MOUNT_POINT="/media/timecapsule"

IS_MOUNTED=`mount 2> /dev/null | grep "$MOUNT_POINT" | cut -d' ' -f3`
TIMECAPSULE_PATH="//$TIMECAPSULE_IP$TIMECAPSULE_VOLUME"

if [[ "$IS_MOUNTED" ]] ;then
	sudo umount "$MOUNT_POINT"
	sudo rmdir "$MOUNT_POINT"
else
	sudo mkdir "$MOUNT_POINT"
	sudo mount -t cifs "$TIMECAPSULE_PATH" "$MOUNT_POINT" -o guest,file_mode=0777,dir_mode=0777,sec=ntlm,vers=1.0
fi