#!/bin/bash

if [[ -d "$HOME/Nextcloud/Backup" ]]; then
    backup_folder="$HOME/Nextcloud/Backup/$(hostname)/Backup"
else
    backup_folder="$HOME/Backup"
fi

if [[ -d "$backup_folder"/backup-scripts ]]; then
    chmod +x "$backup_folder"/backup-scripts/*.sh
    "$backup_folder"/backup-scripts/*.sh
fi

if [[ -d "$backup_folder" ]]; then
    rm -R "$backup_folder"
    mkdir "$backup_folder"
else
    mkdir "$backup_folder"
fi

if [[ ! -d "$backup_folder"/sources.list.d ]]; then
    mkdir "$backup_folder"/sources.list.d
fi

if [[ ! -d "$backup_folder"/trusted.gpg.d ]]; then
    mkdir "$backup_folder"/trusted.gpg.d
fi

if [[ ! -d "$backup_folder"/keyrings ]]; then
    mkdir "$backup_folder"/keyrings
fi

sudo apt autoremove

sudo dpkg --purge $(dpkg --get-selections | grep deinstall | cut -f1) &>/dev/null
dpkg --get-selections > "$backup_folder"/Package.list

sudo cp /etc/apt/sources.list "$backup_folder"/

for file in /etc/apt/sources.list.d/*; do
    if [[ $file == *.list ]]; then
        sudo cp -R $file "$backup_folder"/sources.list.d
    fi
done

sudo cp /etc/apt/trusted.gpg "$backup_folder"/

for file in /etc/apt/trusted.gpg.d/*; do
    if [[ $file == *.gpg ]]; then
        sudo cp -R $file "$backup_folder"/trusted.gpg.d
    fi
done

for file in /usr/share/keyrings/*; do
    if [[ $file == *.gpg ]]; then
        sudo cp -R $file "$backup_folder"/keyrings
    fi
done

if [[ ! -z "$(which snap)" ]]; then
    snap list | tail -n +2 | sed 's/\ .*//' | sudo tee /tmp/snap_names.txt &>/dev/null
    snap list | tail -n +2 | sed 's/.*latest\///' | sed 's/\ .*//' | sudo tee /tmp/snap_tracking.txt &>/dev/null
    
    paste -d "\ " /tmp/snap_names.txt /tmp/snap_tracking.txt | tee "$backup_folder"/snap.txt &>/dev/null

    sudo rm /tmp/snap_names.txt
    sudo rm /tmp/snap_tracking.txt
fi

if [[ ! -z "$(which pip)" ]]; then
    pip freeze | tee "$backup_folder"/pip.txt
fi

if [[ ! -z "$(which pip3)" ]]; then
    pip3 freeze | tee "$backup_folder"/pip3.txt
fi

if [[ ! -z "$(which npm)" ]]; then
    npm list --global --parseable --depth=0 | sed '1d' | awk '{gsub(/\/.*\//,"",$1); print}' > "$backup_folder"/npm.txt
fi

dconf dump / > "$backup_folder"/dconf.conf

sudo chown -R $USER:$USER "$backup_folder"

if [[ -d "$backup_folder"/post-backup-scripts ]]; then
    chmod +x "$backup_folder"/post-backup-scripts/*.sh
    "$backup_folder"/post-backup-scripts/*.sh
fi