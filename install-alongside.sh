#!/bin/bash

sudo cryptsetup luksFormat --hash=sha512 --key-size=512 /dev/<INDENTIFER_ROOT>
sudo cryptsetup open --type=luks /dev/<INDENTIFER_ROOT> rootfs

sudo pvcreate /dev/mapper/rootfs
sudo vgcreate vgroot /dev/mapper/rootfs
sudo lvcreate -n lvswap -L <SIZE> vgroot
sudo lvcreate -n lvroot -l 100%FREE vgroot

sudo blkid /dev/<INDENTIFER_ROOT>

sudo mount /dev/mapper/vgroot-lvroot /mnt
sudo mount /dev/<INDENTIFER_BOOT> /mnt/boot
sudo mount --bind /dev /mnt/dev
sudo chroot /mnt
mount -t proc proc /proc
mount -t sysfs sys /sys
mount -t devpts devpts /dev/pts

echo rootfs UUID=<UUID_ROOTFS> none luks,discard > /etc/crypttab
echo luks-<UUID_ROOTFS> UUID=<UUID_ROOTFS> none luks,discard >> /etc/crypttab
update-initramfs -k all -c
