#!/bin/bash

if [[ ! -d /root/.gnupg ]]; then
    sudo mkdir /root/.gnupg &>/dev/null
fi

sudo chmod -R 700 /root/.gnupg

sudo gpg --no-default-keyring --keyring /usr/share/keyrings/nextcloud-devs-client.gpg --keyserver keyserver.ubuntu.com --recv-keys 1FCD77DD0DBEF5699AD2610160EE47FBAD3DD469
echo "deb [signed-by=/usr/share/keyrings/nextcloud-devs-client.gpg] http://ppa.launchpad.net/nextcloud-devs/client/ubuntu $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/nextcloud-devs-client.list

sudo gpg --no-default-keyring --keyring /usr/share/keyrings/papirus-papirus.gpg --keyserver keyserver.ubuntu.com --recv-keys E58A9D36647CAE7F
echo "deb [signed-by=/usr/share/keyrings/papirus-papirus.gpg] http://ppa.launchpad.net/papirus/papirus/ubuntu groovy main" | sudo tee /etc/apt/sources.list.d/papirus-papirus.list

sudo gpg --no-default-keyring --keyring /usr/share/keyrings/papirus-papirus-dev.gpg --keyserver keyserver.ubuntu.com --recv-keys 9461999446FAF0DF770BFC9AE58A9D36647CAE7F
echo "deb [signed-by=/usr/share/keyrings/papirus-papirus-dev.gpg] http://ppa.launchpad.net/papirus/papirus-dev/ubuntu groovy main" | sudo tee /etc/apt/sources.list.d/papirus-papirus-dev.list

wget -O- https://updates.signal.org/desktop/apt/keys.asc | gpg --dearmor | sudo tee /usr/share/keyrings/signal-xenial.gpg &>/dev/null
echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/signal-xenial.gpg] https://updates.signal.org/desktop/apt xenial main" | sudo tee /etc/apt/sources.list.d/signal-xenial.list

sudo wget -O /usr/share/keyrings/riot-im.gpg https://packages.riot.im/debian/riot-im-archive-keyring.gpg
echo "deb [signed-by=/usr/share/keyrings/riot-im.gpg] https://packages.riot.im/debian/ default main" | sudo tee /etc/apt/sources.list.d/riot-im.list

wget -O- https://deb.nodesource.com/gpgkey/nodesource.gpg.key | gpg --dearmor | sudo tee /usr/share/keyrings/nodesource.gpg &>/dev/null
echo "deb [signed-by=/usr/share/keyrings/nodesource.gpg] https://deb.nodesource.com/node_15.x $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/nodesource.list

sudo wget -O /usr/share/keyrings/julian-fairfax-apt-key.gpg https://gitlab.com/julianfairfax/apt-repo/-/raw/master/apt-key.gpg
echo "deb [signed-by=/usr/share/keyrings/julian-fairfax-apt-key.gpg] https://julianfairfax.gitlab.io/apt-repo/ ./" | sudo tee /etc/apt/sources.list.d/julian-fairfax-apt-repo.list

wget -O- https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/master/pub.gpg | gpg --dearmor | sudo tee /usr/share/keyrings/vscodium.gpg &>/dev/null
echo 'deb [signed-by=/usr/share/keyrings/vscodium.gpg] https://paulcarroty.gitlab.io/vscodium-deb-rpm-repo/debs/ vscodium main' | sudo tee /etc/apt/sources.list.d/vscodium.list 

sudo gpg --no-default-keyring --keyring /usr/share/keyrings/inkscape.dev-trunk.gpg --keyserver keyserver.ubuntu.com --recv-keys 22C4850146603F3DB0ED00479DA4BD18B9A06DE3
echo "deb [signed-by=/usr/share/keyrings/inkscape.dev-trunk.gpg] http://ppa.launchpad.net/inkscape.dev/trunk/ubuntu $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/inkscape.dev-trunk.list

sudo gpg --no-default-keyring --keyring /usr/share/keyrings/caffeine-developers-ppa.gpg --keyserver keyserver.ubuntu.com --recv-keys DC2887936B4153CA9AFB151B0F678A01569113AE
echo "deb [signed-by=/usr/share/keyrings/caffeine-developers-ppa.gpg] http://ppa.launchpad.net/caffeine-developers/ppa/ubuntu $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/caffeine-developers-ppa.list

sudo gpg --no-default-keyring --keyring /usr/share/keyrings/nicotine-team-stable.gpg --keyserver keyserver.ubuntu.com --recv-keys 6E60F93DCD3E27CBE2F0CCA16CEB6050A30E5769
echo "deb [signed-by=/usr/share/keyrings/nicotine-team-stable.gpg] http://ppa.launchpad.net/nicotine-team/stable/ubuntu $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/nicotine-team-stable.list

wget -O- https://static.geogebra.org/linux/office@geogebra.org.gpg.key | gpg --dearmor | sudo tee /usr/share/keyrings/geogebra.gpg &>/dev/null
echo "deb [signed-by=/usr/share/keyrings/geogebra.gpg] http://www.geogebra.net/linux/ stable main" | sudo tee /etc/apt/sources.list.d/geogebra.list

wget -O- https://repo.protonvpn.com/debian/public_key.asc | gpg --dearmor | sudo tee /usr/share/keyrings/protonvpn.gpg &>/dev/null
echo "deb [signed-by=/usr/share/keyrings/protonvpn.gpg] https://repo.protonvpn.com/debian unstable main" | sudo tee /etc/apt/sources.list.d/protonvpn.list

sudo gpg --no-default-keyring --keyring /usr/share/keyrings/phoerious-keepassxc.gpg --keyserver keyserver.ubuntu.com --recv-keys D89C66D0E31FEA2874EBD20561922AB60068FCD6
echo "deb [signed-by=/usr/share/keyrings/phoerious-keepassxc.gpg] http://ppa.launchpad.net/phoerious/keepassxc/ubuntu $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/phoerious-keepassxc.list

sudo apt update