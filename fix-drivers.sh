echo "cat << EOF
setpci -s "00:17.0" 3e.b=8
setpci -s "02:00.0" 04.b=7
EOF" | sudo tee /etc/grub.d/01_enable_vga.conf

sudo chmod 755 /etc/grub.d/01_enable_vga.conf
sudo update-grub

echo 'Section "Device"
    Identifier     "Device0"
    Option         "RegistryDwords" "EnableBrightnessControl=1"
EndSection' | sudo tee /usr/share/X11/xorg.conf.d/10-brightness.conf

echo 'Section "Screen"
    Identifier     "Screen0"
    Option         "NoLogo" "True"
EndSection' | sudo tee /usr/share/X11/xorg.conf.d/10-nologo.conf
