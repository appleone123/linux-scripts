#!/bin/bash

sudo apt install qt5-style-plugins -y

if [[ ! -d ~/.config/environment.d ]]; then
    mkdir -p ~/.config/environment.d
fi

echo "[Qt]
style=GTK+" | tee -a ~/.config/Trolltech.conf

echo "QT_QPA_PLATFORMTHEME=gtk2
QT_STYLE_OVERRIDE=gtk2" | tee -a ~/.config/environment.d/envvars.conf