#!/bin/bash

if [[ ! -d ~/.local/share/applications ]]; then
    mkdir ~/.local/share/applications
fi

cp /usr/share/applications/signal-desktop.desktop ~/.local/share/applications
sed -i 's/\/opt\/Signal\/signal\-desktop\ \-\-no\-sandbox\ \%U/\/opt\/Signal\/signal\-desktop\ \-\-no\-sandbox\ \-\-use\-tray\-icon\ \%U/' ~/.local/share/applications/signal-desktop.desktop

echo "[Desktop Entry]
Type=Application
Exec=signal-desktop --start-in-tray
Icon=signal-desktop
Hidden=false
NoDisplay=false
X-GNOME-Autostart-enabled=true
Name[en_US]=Signal
Name=Signal
Comment[en_US]=
Comment=" | tee ~/.config/autostart/signal-desktop.desktop