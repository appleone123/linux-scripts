#!/bin/bash

sudo cryptsetup open --type=luks /dev/<INDENTIFER_ROOT> rootfs

sudo mount /dev/mapper/vgubuntu-root /mnt
sudo mount /dev/<INDENTIFER_BOOT> /mnt/boot
sudo mount /dev/<INDENTIFER_EFI> /mnt/boot/efi
sudo mount --bind /dev /mnt/dev
sudo chroot /mnt
mount -t proc proc /proc
mount -t sysfs sys /sys
mount -t devpts devpts /dev/pts

grub-install /dev/<INDENTIFER_DISK>
update-grub
