#!/bin/bash

if [[ ! -d ~/.usr/lib/android-sdk ]]; then
    mkdir -p ~/.usr/lib/android-sdk
fi

wget -O ~/Downloads/commandlinetools-linux_latest.zip https://dl.google.com/android/repository/commandlinetools-linux-7302050_latest.zip
unzip -d ~/Downloads ~/Downloads/commandlinetools-linux_latest.zip
rm ~/Downloads/commandlinetools-linux_latest.zip
mv ~/Downloads/cmdline-tools/ ~/.usr/lib/android-sdk/tools
yes | ~/.usr/lib/android-sdk/tools/bin/sdkmanager --licenses --sdk_root=/home/julian/.usr/lib/android-sdk