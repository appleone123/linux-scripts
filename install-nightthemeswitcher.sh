#!/bin/bash

if [[ ! -d ~/.local/share/gnome-shell/extensions/nightthemeswitcher@romainvigier.fr ]]; then
    git clone https://gitlab.com/rmnvgr/nightthemeswitcher-gnome-shell-extension.git
    cd nightthemeswitcher-gnome-shell-extension
    make build
    make install
    cd ../
    rm -rf nightthemeswitcher-gnome-shell-extension
fi