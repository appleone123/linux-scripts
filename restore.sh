#!/bin/bash

if [[ -d "$HOME/Nextcloud/Backup" ]]; then
    backup_folder="$HOME/Nextcloud/Backup/$(hostname)/Backup"
else
    backup_folder="$HOME/Backup"
fi

if [[ -d "$backup_folder"/restore-scripts ]]; then
    chmod +x "$backup_folder"/restore-scripts/*.sh
    "$backup_folder"/restore-scripts/*.sh
fi

sudo cp "$backup_folder"/sources.list /etc/apt/

for file in "$backup_folder"/sources.list.d/*; do
    if [[ $file == *.list ]]; then
        sudo cp -R $file /etc/apt/sources.list.d
    fi
done

sudo cp "$backup_folder"/trusted.gpg /etc/apt/

for file in "$backup_folder"/trusted.gpg.d/*; do
    if [[ $file == *.gpg ]]; then
        sudo cp -R $file /etc/apt/trusted.gpg.d
    fi
done

for file in "$backup_folder"/keyrings/*; do
    if [[ $file == *.gpg ]]; then
        sudo cp -R $file /usr/share/keyrings
    fi
done

export DEBIAN_FRONTEND=noninteractive

sudo apt update
sudo apt install dselect -y

sudo apt-cache dumpavail > /tmp/temp_avail
sudo dpkg --merge-avail /tmp/temp_avail

sudo rm /tmp/temp_avail

sudo dpkg --set-selections < "$backup_folder"/Package.list
sudo apt-get dselect-upgrade

if [[ ! -z "$(which snap)" ]]; then
    while read snap; do
        echo "snap install "$(echo "$snap" | sed 's/\ /\ --/')"" | sudo /bin/bash
    done < "$backup_folder"/snap.txt
fi

if [[ ! -z "$(which pip)" ]]; then
    pip install -r "$backup_folder"/pip.txt
fi

if [[ ! -z "$(which pip3)" ]]; then
    if [[ ! -z $(diff "$backup_folder"/pip.txt "$backup_folder"/pip3.txt) ]]; then
        pip3 install -r "$backup_folder"/pip3.txt
    fi
fi

if [[ ! -z "$(which npm)" ]]; then
    sudo npm install --global <  "$backup_folder"/npm.txt
fi


dconf load / < "$backup_folder"/dconf.conf

if [[ -d "$backup_folder"/post-restore-scripts ]]; then
    chmod +x "$backup_folder"/post-restore-scripts/*.sh
    "$backup_folder"/post-restore-scripts/*.sh
fi